const sortcar = (inventory) => {
  if (!inventory) return [];
  return inventory.sort((a, b) => {
    var x = a.car_model.toUpperCase();
    var y = b.car_model.toUpperCase();
    if (x < y) {
      return -1;
    }
    if (x > y) {
      return 1;
    }

    // names must be equal
    return 0;
  });
};

module.exports = sortcar;