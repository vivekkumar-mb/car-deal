function lastcar(inventory) {
    if (!inventory) return [];
    let lastcar = inventory.length - 1;
    return inventory[lastcar];
}

module.exports = lastcar;