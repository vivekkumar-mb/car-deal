function getCarinfo(inven, carid) {
    if (!inven || !carid) return [];
    for (let i = 0; i < inven.length; i++) {
        if (inven[i]['id'] === carid) {

            return inven[i];
        }
    }
    return [];
}
module.exports = getCarinfo;