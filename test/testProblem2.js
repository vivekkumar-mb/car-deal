const inventory = require("./cardata.js");
const lastcar = require("../problem2.js");

const lastcardetails = lastcar(inventory);
if (lastcardetails)
    console.log(`Last car is a ${lastcardetails.car_make} ${lastcardetails.car_model}`);
else
    console.log("NO CARS FOUND");