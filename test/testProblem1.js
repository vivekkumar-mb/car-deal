const inventory = require("./cardata.js");
const getCarinfo = require("../problem1.js");
let id = 33;
const carDetails = getCarinfo(inventory, id);
if (carDetails)
    console.log(`Car ${carDetails.id} is a ${carDetails.car_year} ${carDetails.car_make} ${carDetails.car_model}`);
else
    console.log("NO CARS FOUND");